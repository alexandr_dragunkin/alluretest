package ru.kolya_iks;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.*;
import ru.yandex.qatools.allure.annotations.*;



public class AppTest {

    @Parameter("Operand1")
    private int Operand1;
    @Parameter("Operand2")
    private int Operand2;
    @Parameter("Operachia")
    private String Operachia;
    @Parameter("Rezult")
    private int Rezult;


    private App app;

    @Before
    public void init() { app = new App(); }
    @After
    public void tearDown() { app = null; }

    @Title("a + b")
    @Test
    public void testPlus()
    {
        app.setArrayList();

        int a = Integer.parseInt(app.arrayList.get(0)); //операнд 1
        int b = Integer.parseInt(app.arrayList.get(1)); //операнд 2
        String c = app.arrayList.get(2); //операция
        int d = Integer.parseInt(app.arrayList.get(3)); //результат

        Operand1=a;
        Operand2=b;
        Operachia=c;
        Rezult=d;
        if (c.equals("+")) assertTrue((a+b)==d);

    }

    @Title("a - b")
    @Test
    public void testMinus()
    {
        app.setArrayList();

        int a = Integer.parseInt(app.arrayList.get(4)); //операнд 3
        int b = Integer.parseInt(app.arrayList.get(5)); //операнд 4
        String c = app.arrayList.get(6); //операция
        int d = Integer.parseInt(app.arrayList.get(7)); //результат

        Operand1=a;
        Operand2=b;
        Operachia=c;
        Rezult=d;
        if (c.equals("-")) assertTrue((a-b)==d);

    }

    @Title("a / b")
    @Test
    public void testDivide()
    {
        app.setArrayList();

        int a = Integer.parseInt(app.arrayList.get(8)); //операнд 5
        int b = Integer.parseInt(app.arrayList.get(9)); //операнд 6
        String c = app.arrayList.get(10); //операция
        int d = Integer.parseInt(app.arrayList.get(11)); //результат

        Operand1=a;
        Operand2=b;
        Operachia=c;
        Rezult=d;
        if (c.equals("/")) assertTrue((a/b)==d);

    }

    @Title("a * b")
    @Test
    public void testMultiply()
    {
        app.setArrayList();

        int a = Integer.parseInt(app.arrayList.get(12)); //операнд 7
        int b = Integer.parseInt(app.arrayList.get(13)); //операнд 8
        String c = app.arrayList.get(14); //операция
        int d = Integer.parseInt(app.arrayList.get(15)); //результат

        Operand1=a;
        Operand2=b;
        Operachia=c;
        Rezult=d;
        if (c.equals("*")) assertTrue((a*b)==d);

    }



 }

